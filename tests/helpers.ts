import { DateTime, Settings } from 'luxon'
import nock from 'nock'
import * as Client from '../src/Client.js'

export const apiKey = 'da-key'
export const apiSecret = 'da-secret'
export const nowISO = '2022-08-29T01:23:45.000Z'
export const nowMillis = DateTime.fromISO(nowISO).toMillis()

export const initialize = () => {
  Client.initialize({ key: apiKey, secret: apiSecret })
  Settings.now = () => nowMillis
}

export const mockRequestV2 = () =>
  nock('https://api.curios.com/v2/api', {
    reqheaders: {
      'curios-api-key': apiKey,
    },
  })

export const mockRequestV3 = () =>
  nock('https://api-v3.curios.com', {
    reqheaders: {
      'curios-api-key': apiKey,
      'curios-date': nowISO,
    },
  })

export const mockRequestAdmin = () =>
  nock('https://admin-api.curios.com', {
    reqheaders: {
      'curios-api-key': apiKey,
      'curios-date': nowISO,
    },
  })
