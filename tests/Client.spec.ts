import { Group, test } from '@japa/runner'
import * as Client from '../src/Client.js'
import { initialize, mockRequestAdmin, mockRequestV2, mockRequestV3 } from './helpers.js'

function groupInit(group: Group) {
  group.each.setup(initialize)
}

test.group('Client v2', group => {
  groupInit(group)

  // @ts-ignore
  test('GET success true returns data', async ({ assert }) => {
    mockV2Get().reply(200, () => {
      return { success: true, errors: [], data: { foo: 'bar' } }
    })

    const actual = await Client.v2.get('good')

    assert.deepEqual(actual, { foo: 'bar' })
  })

  // @ts-ignore
  test('GET success false raises error', async ({ assert }) => {
    assert.plan(1)
    mockV2Get().reply(200, () => {
      return { success: false, message: 'API error message', data: {} }
    })

    try {
      await Client.v2.get('good')
    } catch (error: any) {
      assert.equal(error.message, 'API error message')
    }
  })

  // @ts-ignore
  test('GET non 200 status raises error', async ({ assert }, { status, message }) => {
    assert.plan(1)
    mockV2Get().reply(status, () => {
      return { success: false, message: 'API error message', data: {} }
    })

    try {
      await Client.v2.get('good')
    } catch (error: any) {
      assert.equal(error.message, message)
    }
  }).with([
    { status: 400, message: 'Request failed with status code 400' },
    { status: 401, message: 'Request failed with status code 401' },
    { status: 404, message: 'Request failed with status code 404' },
    { status: 500, message: 'Request failed with status code 500' },
    { status: 529, message: 'Request failed with status code 529' },
  ])

  // @ts-ignore
  test('POST success true returns data', async ({ assert }) => {
    mockV2Post().reply(200, () => {
      return { success: true, errors: [], data: { foo: 'bar' } }
    })

    const actual = await Client.v2.post('op', { rocket: 'surgery' })

    assert.deepEqual(actual, { foo: 'bar' })
  })

  // @ts-ignore
  test('POST success false raises error', async ({ assert }) => {
    assert.plan(1)
    mockV2Post().reply(200, () => {
      return { success: false, message: 'API error message', data: {} }
    })

    try {
      await Client.v2.post('op', { rocket: 'surgery' })
    } catch (error: any) {
      assert.equal(error.message, 'API error message')
    }
  })

  // @ts-ignore
  test('POST non 200 status raises error', async ({ assert }, { status, message }) => {
    assert.plan(1)
    mockV2Post().reply(status, () => {
      return { success: false, message: 'API error message', data: {} }
    })

    try {
      await Client.v2.post('op', { rocket: 'surgery' })
    } catch (error: any) {
      assert.equal(error.message, message)
    }
  }).with([
    { status: 400, message: 'Request failed with status code 400' },
    { status: 401, message: 'Request failed with status code 401' },
    { status: 404, message: 'Request failed with status code 404' },
    { status: 500, message: 'Request failed with status code 500' },
    { status: 529, message: 'Request failed with status code 529' },
  ])
})

test.group('Client v3', group => {
  groupInit(group)

  // @ts-ignore
  test('GET success true returns data', async ({ assert }) => {
    mockV3Get().reply(200, () => {
      return { SUCCESS: true, ERRORS: [], DATA: { foo: 'bar' } }
    })

    const actual = await Client.v3.get('good')

    assert.deepEqual(actual, { foo: 'bar' })
  })

  // @ts-ignore
  test('GET success false raises error', async ({ assert }) => {
    assert.plan(1)
    mockV3Get().reply(200, () => {
      return { SUCCESS: false, MESSAGE: 'API error message', DATA: {} }
    })

    try {
      await Client.v3.get('good')
    } catch (error: any) {
      assert.equal(error.message, 'API error message')
    }
  })

  // @ts-ignore
  test('GET non 200 status raises error', async ({ assert }, { status, message }) => {
    assert.plan(1)
    mockV3Get().reply(status, () => {
      return { SUCCESS: false, MESSAGE: 'API error message', DATA: {} }
    })

    try {
      await Client.v3.get('good')
    } catch (error: any) {
      assert.equal(error.message, message)
    }
  }).with([
    { status: 400, message: 'Request failed with status code 400' },
    { status: 401, message: 'Request failed with status code 401' },
    { status: 404, message: 'Request failed with status code 404' },
    { status: 500, message: 'Request failed with status code 500' },
    { status: 529, message: 'Request failed with status code 529' },
  ])

  // @ts-ignore
  test('POST success true returns data', async ({ assert }) => {
    mockV3Post().reply(200, () => {
      return { SUCCESS: true, ERRORS: [], DATA: { foo: 'bar' } }
    })

    const actual = await Client.v3.post('op', { rocket: 'surgery' })

    assert.deepEqual(actual, { foo: 'bar' })
  })

  // @ts-ignore
  test('POST success false raises error', async ({ assert }) => {
    assert.plan(1)
    mockV3Post().reply(200, () => {
      return { SUCCESS: false, MESSAGE: 'API error message', DATA: {} }
    })

    try {
      await Client.v3.post('op', { rocket: 'surgery' })
    } catch (error: any) {
      assert.equal(error.message, 'API error message')
    }
  })

  // @ts-ignore
  test('POST non 200 status raises error', async ({ assert }, { status, message }) => {
    assert.plan(1)
    mockV3Post().reply(status, () => {
      return { SUCCESS: false, MESSAGE: 'API error message', DATA: {} }
    })

    try {
      await Client.v3.post('op', { rocket: 'surgery' })
    } catch (error: any) {
      assert.equal(error.message, message)
    }
  }).with([
    { status: 400, message: 'Request failed with status code 400' },
    { status: 401, message: 'Request failed with status code 401' },
    { status: 404, message: 'Request failed with status code 404' },
    { status: 500, message: 'Request failed with status code 500' },
    { status: 529, message: 'Request failed with status code 529' },
  ])
})

test.group('Client admin', group => {
  groupInit(group)

  // @ts-ignore
  test('GET success true returns data', async ({ assert }) => {
    mockAdminGet().reply(200, () => {
      return { SUCCESS: true, ERRORS: [], DATA: { foo: 'bar' } }
    })

    const actual = await Client.admin.get('good')

    assert.deepEqual(actual, { foo: 'bar' })
  })

  // @ts-ignore
  test('GET success false raises error', async ({ assert }) => {
    assert.plan(1)
    mockAdminGet().reply(200, () => {
      return { SUCCESS: false, MESSAGE: 'API error message', DATA: {} }
    })

    try {
      await Client.admin.get('good')
    } catch (error: any) {
      assert.equal(error.message, 'API error message')
    }
  })

  // @ts-ignore
  test('GET non 200 status raises error', async ({ assert }, { status, message }) => {
    assert.plan(1)
    mockAdminGet().reply(status, () => {
      return { SUCCESS: false, MESSAGE: 'API error message', DATA: {} }
    })

    try {
      await Client.admin.get('good')
    } catch (error: any) {
      assert.equal(error.message, message)
    }
  }).with([
    { status: 400, message: 'Request failed with status code 400' },
    { status: 401, message: 'Request failed with status code 401' },
    { status: 404, message: 'Request failed with status code 404' },
    { status: 500, message: 'Request failed with status code 500' },
    { status: 529, message: 'Request failed with status code 529' },
  ])

  // @ts-ignore
  test('POST success true returns data', async ({ assert }) => {
    mockAdminPost().reply(200, () => {
      return { SUCCESS: true, ERRORS: [], DATA: { foo: 'bar' } }
    })

    const actual = await Client.admin.post('op', { rocket: 'surgery' })

    assert.deepEqual(actual, { foo: 'bar' })
  })

  // @ts-ignore
  test('POST success false raises error', async ({ assert }) => {
    assert.plan(1)
    mockAdminPost().reply(200, () => {
      return { SUCCESS: false, MESSAGE: 'API error message', DATA: {} }
    })

    try {
      await Client.admin.post('op', { rocket: 'surgery' })
    } catch (error: any) {
      assert.equal(error.message, 'API error message')
    }
  })

  // @ts-ignore
  test('POST non 200 status raises error', async ({ assert }, { status, message }) => {
    assert.plan(1)
    mockAdminPost().reply(status, () => {
      return { SUCCESS: false, MESSAGE: 'API error message', DATA: {} }
    })

    try {
      await Client.admin.post('op', { rocket: 'surgery' })
    } catch (error: any) {
      assert.equal(error.message, message)
    }
  }).with([
    { status: 400, message: 'Request failed with status code 400' },
    { status: 401, message: 'Request failed with status code 401' },
    { status: 404, message: 'Request failed with status code 404' },
    { status: 500, message: 'Request failed with status code 500' },
    { status: 529, message: 'Request failed with status code 529' },
  ])
})

const mockV2Get = () =>
  mockRequestV2()
    .get('/good')
    .matchHeader('curios-signature', '41d8afc8c178707045c1889fd622d4e030455064b09eff08c7df67f49cf08459')

const mockV2Post = () =>
  mockRequestV2()
    .post('/op', { rocket: 'surgery' })
    .matchHeader('curios-signature', 'f44ce7bab9d8a727bfbead6cef4dcbdea4ec6b1b0f837d536b42f57edeeef6e9')

const mockV3Get = () =>
  mockRequestV3()
    .get('/good')
    .matchHeader('curios-signature', '664f8b7bc536d26beeae44617e84dceec9c1ad90eb34782586f92b99c0ab090c')

const mockV3Post = () =>
  mockRequestV3()
    .post('/op', { rocket: 'surgery' })
    .matchHeader('curios-signature', 'd311772ebf24680cb24d03bc3b83cf3cc3da4945833a7551d1760fb3ed520049')

const mockAdminGet = () =>
  mockRequestAdmin()
    .get('/good')
    .matchHeader('curios-signature', '664f8b7bc536d26beeae44617e84dceec9c1ad90eb34782586f92b99c0ab090c')

const mockAdminPost = () =>
  mockRequestAdmin()
    .post('/op', { rocket: 'surgery' })
    .matchHeader('curios-signature', 'd311772ebf24680cb24d03bc3b83cf3cc3da4945833a7551d1760fb3ed520049')
