import { faker } from '@faker-js/faker'
import { test } from '@japa/runner'
import { DateTime } from 'luxon'
import nock from 'nock'
import * as qs from 'node:querystring'
import { Airdrops, SortDir } from '../src/index.js'
import { Airdrop, AllParams, AllSortBy, NewUserAction } from '../src/Airdrops.js'
import { initialize, mockRequestAdmin } from './helpers.js'

test.group('Airdrops', group => {
  group.each.setup(initialize)
  // @ts-ignore
  test('get all with default params', async ({ assert }) => {
    const airdrops = [makeAirdrop(), makeAirdrop(), makeAirdrop()]
    mockAllGet('', airdrops)

    const actual = await Airdrops.all()

    assert.deepEqual(actual, airdrops)
  })

  // @ts-ignore
  test('get all with some params', async ({ assert }) => {
    const airdrops = [makeAirdrop()]
    const params = {
      max_results: 40,
      results_after: fakeAddress(),
    } as Partial<AllParams>
    mockAllGet('?' + qs.stringify(params), airdrops)

    const actual = await Airdrops.all(params)

    assert.deepEqual(actual, airdrops)
  })

  // @ts-ignore
  test('get all with all params', async ({ assert }) => {
    const airdrops = [makeAirdrop(), makeAirdrop()]
    const params = {
      max_results: 3,
      results_after: fakeAddress(),
      sort_by: AllSortBy.token_id,
      sort_dir: SortDir.desc,
      new_user_action: NewUserAction.invite,
      send_emails: false,
    } as Partial<AllParams>
    mockAllGet('?' + qs.stringify(params), airdrops)

    const actual = await Airdrops.all(params)

    assert.deepEqual(actual, airdrops)
  })

  // @ts-ignore
  test('get details', async ({ assert }) => {
    const airdrop = makeAirdrop({ ID: '0x0xd2e01C3ee74ba276ea2eFFCFCCEfDA3B' })
    mockDetailsGet(airdrop)

    const actual = await Airdrops.details(airdrop.ID)

    assert.deepEqual(actual, airdrop)
  })

  // @ts-ignore
  test('create success true', async ({ assert }) => {
    const body = {
      reason: 'really good reason',
      collection_id: '0x0xAe7BA9263119dd0DcF4A1DBe8C6457Af',
      new_user_action: NewUserAction.invite,
      send_emails: false,
    }
    const expected = { id: fakeAddress() }
    mockRequestAdmin()
      .matchHeader('curios-signature', '28a2345d060a94d0f71ed325185b13fe77206ce907ac8e8fe608a61b0b2cfd33')
      .post('/airdrops', body)
      .reply(200, () => {
        return { SUCCESS: true, ERRORS: [], DATA: expected }
      })

    const actual = await Airdrops.create({
      reason: body.reason,
      collection_id: body.collection_id,
      new_user_action: body.new_user_action,
      send_emails: body.send_emails,
    })

    assert.deepEqual(actual, expected.id)
  })

  // @ts-ignore
  test('create success false', async ({ assert }) => {
    assert.plan(1)
    const body = {
      reason: 'fail',
      collection_id: fakeAddress(),
      new_user_action: NewUserAction.register,
      send_emails: true,
    }
    nock('https://admin-api.curios.com')
      .post('/airdrops', body)
      .reply(200, () => {
        return { SUCCESS: false, MESSAGE: 'API error message', DATA: {} }
      })

    try {
      await Airdrops.create({
        reason: body.reason,
        collection_id: body.collection_id,
        new_user_action: body.new_user_action,
        send_emails: body.send_emails,
      })
    } catch (error: any) {
      assert.equal(error.message, 'API error message')
    }
  })
})

const fakeAddress = (): string => `0x${faker.datatype.hexadecimal(32)}`

const jsDateToDTS = (date: Date): string => DateTime.fromJSDate(date).toFormat('DDD TT')

const makeAirdrop = (params: Partial<Airdrop> = {}): Airdrop => {
  const created = faker.date.past()
  return {
    ...{
      ID: fakeAddress(),
      COLLECTION_ID: fakeAddress(),
      REASON: faker.lorem.sentence(),
      TYPE: 'Single',
      CODES: [],
      RECIPIENTS: [],
      NUM_DROPS_FAILED: faker.datatype.number({ min: 2, max: 500 }),
      NUM_DROPS_SUCCEEDED: faker.datatype.number({ min: 2, max: 500 }),
      NEW_USER_ACTION: faker.helpers.arrayElement([NewUserAction.invite, NewUserAction.register]),
      SEND_EMAILS: faker.datatype.boolean(),
      DTS_CREATED: jsDateToDTS(created),
      DTS_DROPPED: jsDateToDTS(faker.date.between(created, 'now')),
    },
    ...params,
  }
}

const mockAllGet = (query: string, airdrops: Airdrop[]) =>
  mockRequestAdmin()
    .matchHeader('curios-signature', '28a2345d060a94d0f71ed325185b13fe77206ce907ac8e8fe608a61b0b2cfd33')
    .get(`/airdrops${query}`)
    .reply(200, () => {
      return { SUCCESS: true, ERRORS: [], DATA: airdrops }
    })

const mockDetailsGet = (airdrop: Airdrop) =>
  mockRequestAdmin()
    .matchHeader('curios-signature', 'daae3344c39c8d77d50795e82e3d3fabaf9325c25f0990897207a06f722f4eee')
    .get(`/airdrops/0x0xd2e01C3ee74ba276ea2eFFCFCCEfDA3B`)
    .reply(200, () => {
      return { SUCCESS: true, ERRORS: [], DATA: airdrop }
    })
