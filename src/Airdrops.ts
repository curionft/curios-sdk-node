import { admin as client } from './Client.js'
import { SortDir } from './index.js'

// noinspection JSUnusedGlobalSymbols
export default class Airdrops {
  /**
   * Fetches list of all airdrops.
   * @throws {Error} When request fails
   */
  public static all = (params: Partial<AllParams> = {}): Promise<Airdrop[]> => client.get('airdrops', { params })

  /**
   * Creates an airdrop.
   * @throws {Error} When request fails
   */
  public static create = async (data: Partial<CreateParams>): Promise<Airdrop> => {
    // @ts-ignore
    const { id } = await client.post('airdrops', data)
    return id
  }

  /**
   * Provides details of a given airdrop matching an ID.
   * @throws {Error} When request fails
   */
  public static details = (airdrop_id: string): Promise<Airdrop> => client.get(`airdrops/${airdrop_id}`)

  /**
   * Sends an airdrop to a list of recipients.
   * @throws {Error} When request fails
   */
  public static send = (airdrop_id: string, ...emails: Array<string>): Promise<void> =>
    client.post(`airdrops/${airdrop_id}/send`, {
      recipients: emails.map(address => {
        return { type: 'email', address }
      }),
    })
}

export enum NewUserAction {
  invite = 'invite',
  register = 'register',
}

export interface AllParams {
  /**
   *  Maximum number of results to return.
   *  @default 10
   */
  max_results: number
  /** ID of last row from previous page result. */
  results_after: string
  sort_by: AllSortBy
  sort_dir: SortDir
  new_user_action: NewUserAction
  send_emails: boolean
}

export enum AllSortBy {
  token_id = 'token_id',
  collection_id = 'collection_id',
  edition_num = 'edition_num',
  dts_created = 'dts_created',
}

export interface CreateParams {
  reason: string
  /** Collection hash. */
  collection_id: string
  /**
   * How to handle new users.
   * @default 'register'
   */
  new_user_action?: NewUserAction
  /**
   * FALSE means our system will not send emails out, this means you have to send the emails out yourself.
   * @default true
   */
  auto_drop?: boolean
  /**
   * FALSE means tokens will NOT be auto-dropped, even if the customer exists, you will need to email them the invitation code in order for them to receive the token.
   * @default true
   */
  send_emails?: boolean
  recipients?: Array<string>
}

export interface Airdrop {
  readonly ID: string
  readonly COLLECTION_ID: string
  readonly REASON: string
  /** Airdrop type, ie: "Single" */
  readonly TYPE: string
  readonly CODES: Array<string>
  readonly RECIPIENTS: Array<Recipient>
  readonly NUM_DROPS_FAILED: number
  readonly NUM_DROPS_SUCCEEDED: number
  readonly NEW_USER_ACTION: NewUserAction
  readonly SEND_EMAILS: boolean
  /** Datetime stamp when the drop was created, ie: "July, 19 2022 19:32:33" */
  readonly DTS_CREATED: string
  /** Datetime stamp when the drop occurred, ie: "July, 19 2022 19:32:33" */
  readonly DTS_DROPPED: string
}

export interface Recipient {
  RECIPIENT_DROP_DTS: string
  CUSTOMER_EMAIL: string
  CUSTOMER_ID: number
  TOKEN_ID: string
  RECIPIENT_DROP_MEMO: string
  RECIPIENT_DROP_SUCCESS: boolean
}
