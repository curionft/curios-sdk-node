import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'
import hmacSHA256 from 'crypto-js/hmac-sha256.js'
import sha256 from 'crypto-js/sha256.js'
import { DateTime } from 'luxon'

let key: string
let secret: string

/**
 * Initialize the REST client.
 */
export function initialize(config: InitializeConfig) {
  key = config.key
  secret = config.secret
}

export type InitializeConfig = {
  key: string
  secret: string
}

function addCommonHeaders(config: AxiosRequestConfig, date: string) {
  // @ts-ignore
  config.headers['curios-api-key'] = key
  // @ts-ignore
  config.headers['curios-date'] = date

  return config
}

export const v2 = axios.create({
  baseURL: 'https://api.curios.com/v2/api/',
})
v2.interceptors.request.use(function (config: AxiosRequestConfig) {
  const date = DateTime.utc().toISO()
  config = addCommonHeaders(config, date)
  const payloadHash = sha256(JSON.stringify(config.data || {})).toString()
  // @ts-ignore
  config.headers['curios-signature'] = hmacSHA256(`${date} ${config.url} ${payloadHash}`, secret).toString()

  return config
})
v2.interceptors.response.use(function (response: AxiosResponse) {
  let { data } = response
  if (false === data.success) {
    throw new Error(data.message || 'Unsuccessful request')
  }
  return data.data
})

export const v3 = axios.create({
  baseURL: 'https://api-v3.curios.com/',
})
v3.interceptors.request.use(function (config: AxiosRequestConfig) {
  const date = DateTime.utc().toISO()
  config = addCommonHeaders(config, date)
  // @ts-ignore
  config.headers['curios-signature'] = hmacSHA256(`${date} ${config.url}`, secret).toString()

  return config
})
v3.interceptors.response.use(function (response: AxiosResponse) {
  let { data } = response
  if (false === (data.success || data.SUCCESS)) {
    throw new Error(data.message || data.MESSAGE || 'Unsuccessful request')
  }
  // normalize capitalization of response structure
  return data.data || data.DATA
})

export const admin = axios.create({
  baseURL: 'https://admin-api.curios.com/',
})
admin.interceptors.request.use(function (config: AxiosRequestConfig) {
  const date = DateTime.utc().toISO()
  config = addCommonHeaders(config, date)
  // @ts-ignore
  config.headers['curios-signature'] = hmacSHA256(`${date} ${config.url}`, secret).toString()

  return config
})
admin.interceptors.response.use(function (response: AxiosResponse) {
  let { data } = response
  if (false === (data.success || data.SUCCESS)) {
    throw new Error(data.message || data.MESSAGE || 'Unsuccessful request')
  }
  // normalize capitalization of response structure
  return data.data || data.DATA
})
