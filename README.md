# Curios SDK for Node.JS

This handy-dandy little library will help you authenticate requests with the [Curios](https://curios.com) API.
Additionally, each endpoint is grouped into its parent object and has strongly typed parameters as well as return values.

## Usage

Import the module, initialize with the key and secret values, then make your request:

```javascript
import * as Curios from 'curios-sdk'

Curios.initialize({ key: 'your-api-key', secret: 'your-api-secret' })
Curios.Collections.all().then(collections => {
  // do stuff with the list of collections
})
```

## Development

[Yalc](https://npmjs.com/package/yalc) is quite helpful for testing local changes with another local application.
Use the `yalc:*` scripts to publish and/or push your changes to other applications.
