import fs from 'fs'

let skip = process.env.CI !== undefined
skip |= process.env.HUSKY === '0'
skip |= !fs.existsSync('.git')
try {
  if (!skip) {
    import('husky').then(husky => husky.install())
  }
} catch (e) {
  if (e.code !== 'MODULE_NOT_FOUND') {
    throw e
  }
}
