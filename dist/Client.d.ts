/**
 * Initialize the REST client.
 */
export declare function initialize(config: InitializeConfig): void;
export declare type InitializeConfig = {
    key: string;
    secret: string;
};
export declare const v2: import("axios").AxiosInstance;
export declare const v3: import("axios").AxiosInstance;
export declare const admin: import("axios").AxiosInstance;
