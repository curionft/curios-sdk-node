'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var axios = require('axios');
var hmacSHA256 = require('crypto-js/hmac-sha256.js');
var sha256 = require('crypto-js/sha256.js');
var luxon = require('luxon');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var axios__default = /*#__PURE__*/_interopDefaultLegacy(axios);
var hmacSHA256__default = /*#__PURE__*/_interopDefaultLegacy(hmacSHA256);
var sha256__default = /*#__PURE__*/_interopDefaultLegacy(sha256);

/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

var key;
var secret;
/**
 * Initialize the REST client.
 */
function initialize(config) {
    key = config.key;
    secret = config.secret;
}
function addCommonHeaders(config, date) {
    // @ts-ignore
    config.headers['curios-api-key'] = key;
    // @ts-ignore
    config.headers['curios-date'] = date;
    return config;
}
var v2 = axios__default["default"].create({
    baseURL: 'https://api.curios.com/v2/api/',
});
v2.interceptors.request.use(function (config) {
    var date = luxon.DateTime.utc().toISO();
    config = addCommonHeaders(config, date);
    var payloadHash = sha256__default["default"](JSON.stringify(config.data || {})).toString();
    // @ts-ignore
    config.headers['curios-signature'] = hmacSHA256__default["default"]("".concat(date, " ").concat(config.url, " ").concat(payloadHash), secret).toString();
    return config;
});
v2.interceptors.response.use(function (response) {
    var data = response.data;
    if (false === data.success) {
        throw new Error(data.message || 'Unsuccessful request');
    }
    return data.data;
});
var v3 = axios__default["default"].create({
    baseURL: 'https://api-v3.curios.com/',
});
v3.interceptors.request.use(function (config) {
    var date = luxon.DateTime.utc().toISO();
    config = addCommonHeaders(config, date);
    // @ts-ignore
    config.headers['curios-signature'] = hmacSHA256__default["default"]("".concat(date, " ").concat(config.url), secret).toString();
    return config;
});
v3.interceptors.response.use(function (response) {
    var data = response.data;
    if (false === (data.success || data.SUCCESS)) {
        throw new Error(data.message || data.MESSAGE || 'Unsuccessful request');
    }
    // normalize capitalization of response structure
    return data.data || data.DATA;
});
var admin = axios__default["default"].create({
    baseURL: 'https://admin-api.curios.com/',
});
admin.interceptors.request.use(function (config) {
    var date = luxon.DateTime.utc().toISO();
    config = addCommonHeaders(config, date);
    // @ts-ignore
    config.headers['curios-signature'] = hmacSHA256__default["default"]("".concat(date, " ").concat(config.url), secret).toString();
    return config;
});
admin.interceptors.response.use(function (response) {
    var data = response.data;
    if (false === (data.success || data.SUCCESS)) {
        throw new Error(data.message || data.MESSAGE || 'Unsuccessful request');
    }
    // normalize capitalization of response structure
    return data.data || data.DATA;
});

// noinspection JSUnusedGlobalSymbols
var Airdrops = /** @class */ (function () {
    function Airdrops() {
    }
    var _a;
    _a = Airdrops;
    /**
     * Fetches list of all airdrops.
     * @throws {Error} When request fails
     */
    Airdrops.all = function (params) {
        if (params === void 0) { params = {}; }
        return admin.get('airdrops', { params: params });
    };
    /**
     * Creates an airdrop.
     * @throws {Error} When request fails
     */
    Airdrops.create = function (data) { return __awaiter(void 0, void 0, void 0, function () {
        var id;
        return __generator(_a, function (_b) {
            switch (_b.label) {
                case 0: return [4 /*yield*/, admin.post('airdrops', data)];
                case 1:
                    id = (_b.sent()).id;
                    return [2 /*return*/, id];
            }
        });
    }); };
    /**
     * Provides details of a given airdrop matching an ID.
     * @throws {Error} When request fails
     */
    Airdrops.details = function (airdrop_id) { return admin.get("airdrops/".concat(airdrop_id)); };
    /**
     * Sends an airdrop to a list of recipients.
     * @throws {Error} When request fails
     */
    Airdrops.send = function (airdrop_id) {
        var emails = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            emails[_i - 1] = arguments[_i];
        }
        return admin.post("airdrops/".concat(airdrop_id, "/send"), {
            recipients: emails.map(function (address) {
                return { type: 'email', address: address };
            }),
        });
    };
    return Airdrops;
}());
var NewUserAction;
(function (NewUserAction) {
    NewUserAction["invite"] = "invite";
    NewUserAction["register"] = "register";
})(NewUserAction || (NewUserAction = {}));
var AllSortBy$1;
(function (AllSortBy) {
    AllSortBy["token_id"] = "token_id";
    AllSortBy["collection_id"] = "collection_id";
    AllSortBy["edition_num"] = "edition_num";
    AllSortBy["dts_created"] = "dts_created";
})(AllSortBy$1 || (AllSortBy$1 = {}));

// noinspection JSUnusedGlobalSymbols
var Collections = /** @class */ (function () {
    function Collections() {
    }
    var _a;
    _a = Collections;
    /**
     * Fetches a list of all NFT collections.
     * @throws {Error} When request fails
     */
    Collections.all = function (params) {
        if (params === void 0) { params = {}; }
        return __awaiter(void 0, void 0, void 0, function () { return __generator(_a, function (_b) {
            return [2 /*return*/, v3.get('collections', { params: params })
                /**
                 * Fetches the details of an NFT collection.
                 * @throws {Error} When request fails
                 */
            ];
        }); });
    };
    /**
     * Fetches the details of an NFT collection.
     * @throws {Error} When request fails
     */
    Collections.details = function (collectionId) { return __awaiter(void 0, void 0, void 0, function () { return __generator(_a, function (_b) {
        return [2 /*return*/, v3.get("collections/".concat(collectionId))
            /**
             * Fetches all sale listings for an NFT collection, both primary and secondary.
             * @throws {Error} When request fails
             */
        ];
    }); }); };
    /**
     * Fetches all sale listings for an NFT collection, both primary and secondary.
     * @throws {Error} When request fails
     */
    Collections.listings = function (collectionId) { return __awaiter(void 0, void 0, void 0, function () { return __generator(_a, function (_b) {
        return [2 /*return*/, v3.get("collections/".concat(collectionId, "/listings"))
            /**
             * Fetches a list of all NFT collection types.
             * @throws {Error} When request fails
             */
        ];
    }); }); };
    /**
     * Fetches a list of all NFT collection types.
     * @throws {Error} When request fails
     */
    Collections.types = function () { return __awaiter(void 0, void 0, void 0, function () { return __generator(_a, function (_b) {
        return [2 /*return*/, v3.get("collections/types")
            /**
             * Fetches the details of an NFT collection type.
             * @throws {Error} When request fails
             */
        ];
    }); }); };
    /**
     * Fetches the details of an NFT collection type.
     * @throws {Error} When request fails
     */
    Collections.typeDetails = function (collectionTypeId) {
        return v3.get("collections/types/".concat(collectionTypeId));
    };
    return Collections;
}());
var AllSortBy;
(function (AllSortBy) {
    AllSortBy[AllSortBy["token_id"] = 0] = "token_id";
    AllSortBy[AllSortBy["collection_id"] = 1] = "collection_id";
    AllSortBy[AllSortBy["edition_num"] = 2] = "edition_num";
    AllSortBy[AllSortBy["dts_created"] = 3] = "dts_created";
})(AllSortBy || (AllSortBy = {}));

exports.SortDir = void 0;
(function (SortDir) {
    SortDir["asc"] = "asc";
    SortDir["desc"] = "desc";
})(exports.SortDir || (exports.SortDir = {}));

exports.Airdrops = Airdrops;
exports.Collections = Collections;
exports.initialize = initialize;
