import { SortDir } from './index.js';
export default class Collections {
    /**
     * Fetches a list of all NFT collections.
     * @throws {Error} When request fails
     */
    static all: (params?: Partial<AllParams>) => Promise<CollectionItem[]>;
    /**
     * Fetches the details of an NFT collection.
     * @throws {Error} When request fails
     */
    static details: (collectionId: string) => Promise<CollectionItem>;
    /**
     * Fetches all sale listings for an NFT collection, both primary and secondary.
     * @throws {Error} When request fails
     */
    static listings: (collectionId: string) => Promise<ListingItem[]>;
    /**
     * Fetches a list of all NFT collection types.
     * @throws {Error} When request fails
     */
    static types: () => Promise<CollectionType[]>;
    /**
     * Fetches the details of an NFT collection type.
     * @throws {Error} When request fails
     */
    static typeDetails: (collectionTypeId: string) => Promise<CollectionType>;
}
export interface AllParams {
    /**
     *  Maximum number of results to return.
     *  @default 10
     */
    max_results: number;
    /** ID of last row from previous page result. */
    results_after: string;
    sort_by: AllSortBy;
    sort_dir: SortDir;
}
export declare enum AllSortBy {
    token_id = 0,
    collection_id = 1,
    edition_num = 2,
    dts_created = 3
}
export interface CollectionItem {
    /** URL to video file */
    readonly VIDEO: string;
    readonly SUBTITLE: string;
    readonly SLUG: string;
    /** URL to audio file */
    readonly AUDIO: string;
    readonly HASH: string;
    /** URL to image */
    readonly IMAGE: string;
    readonly DESCRIPTION: string;
    /** User ID of creator */
    readonly CREATOR: number;
    readonly SORT_ORDER: number;
    /** Number minted */
    readonly MINTAGE_QTY: number;
    readonly TITLE: string;
    /** Type id */
    readonly TYPE: number;
    /** Mint type */
    readonly MINTAGE: string;
    /** Created datetime stamp, ie: "June, 16 2022 18:47:04" */
    readonly DTS_CREATED: string;
    /** Modified datetime stamp, ie: "June, 16 2022 18:58:22" */
    readonly DTS_MODIFIED: string;
    readonly PREVIEW_FILES: readonly PreviewFile[];
}
export interface PreviewFile {
    readonly CREDITS: string;
    readonly IS_PRIMARY: boolean;
    readonly DESCRIPTION: string;
    readonly SORT_ORDER: number;
    readonly LABEL: string;
    readonly THUMBNAIL: string;
    readonly FILE: string;
    readonly TYPE: string;
    /** Created datetime stamp, ie: "June, 16 2022 18:50:33" */
    readonly DTS_CREATED: string;
}
export interface ListingItem {
    readonly LISTING_ID: number;
    readonly LISTING_CUSTOMER_ID: string;
    readonly COLLECTION_ID: string;
    readonly COLLECTION_TITLE: string;
    readonly COLLECTION_SUBTITLE: string;
    readonly COLLECTION_DESCRIPTION: string;
    readonly COLLECTION_PREVIEW_IMAGE: string;
    readonly COLLECTION_PREVIEW_AUDIO: string;
    readonly COLLECTION_PREVIEW_VIDEO: string;
    readonly CURRENCY_CODE: string;
    readonly ITEM_EDITION: string;
    readonly ITEM_TYPE_ID: number;
    readonly PRICE_MIN_BID: number;
    readonly SORT_ORDER: number;
    readonly ACCEPT_CRYPTO: boolean;
    readonly ACCEPT_FIAT: boolean;
    readonly IS_BID: boolean;
    readonly IS_BUY_NOW: boolean;
    readonly IS_FEATURED: boolean;
    readonly IS_SECONDARY: boolean;
    readonly PRICE_BUY_NOW: boolean;
    readonly REQUIRE_FUNDS_VERIFICATION: boolean;
    readonly REQUIRE_KYC: boolean;
    readonly REQUIRE_PASSWORD: boolean;
    /** Optional password to restrict access */
    readonly ACCESS_PASSWORD: string;
    /** Created datetime stamp, ie: "June, 16 2022 18:55:23" */
    readonly DTS_CREATED: string;
    /** Created datetime stamp, ie: "July, 06 2022 19:57:52" */
    readonly DTS_MODIFIED: string;
    /** Created datetime stamp, ie: "July, 14 2022 16:29:44" */
    readonly CURRENT_DTS: string;
    /** Availability start datetime stamp, ie: "June, 30 2022 12:00:00" */
    readonly DTS_START: string;
    /** Availability end datetime stamp, ie: "July, 14 2022 16:29:44" */
    readonly DTS_END: string;
}
export interface CollectionType {
    readonly id: string;
    readonly label: string;
    /** Created datetime stamp, ie: "June, 16 2022 18:55:23" */
    readonly dts_created: string;
    /** Created datetime stamp, ie: "July, 06 2022 19:57:52" */
    readonly dts_modified: string;
}
