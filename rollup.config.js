import typescript from 'rollup-plugin-typescript2'

const external = ['axios', 'crypto-js/hmac-sha256.js', 'crypto-js/sha256.js', 'luxon']

// noinspection JSUnusedGlobalSymbols
export default [
  {
    input: './src/index.ts',
    output: {
      exports: 'auto',
      file: './dist/index.mjs',
      format: 'esm',
    },
    plugins: [typescript()],
    external,
  },
  {
    input: './src/index.ts',
    output: {
      exports: 'auto',
      file: './dist/index.cjs',
      format: 'cjs',
    },
    plugins: [typescript()],
    external,
  },
]
